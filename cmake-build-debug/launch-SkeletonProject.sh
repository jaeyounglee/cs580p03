#!/bin/sh
bindir=$(pwd)
cd /Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/Skeleton/
export 

if test "x$1" = "x--debugger"; then
	shift
	if test "x" = "xYES"; then
		echo "r  " > $bindir/gdbscript
		echo "bt" >> $bindir/gdbscript
		GDB_COMMAND-NOTFOUND -batch -command=$bindir/gdbscript  /Users/jaeyounglee/Documents/Computer\ Graphics-2018-s/Homework3/cmake-build-debug/SkeletonProject 
	else
		"/Users/jaeyounglee/Documents/Computer\ Graphics-2018-s/Homework3/cmake-build-debug/SkeletonProject"  
	fi
else
	"/Users/jaeyounglee/Documents/Computer\ Graphics-2018-s/Homework3/cmake-build-debug/SkeletonProject"  
fi
