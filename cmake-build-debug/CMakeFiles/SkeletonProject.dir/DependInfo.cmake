# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/Skeleton/main.cpp" "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/CMakeFiles/SkeletonProject.dir/Skeleton/main.cpp.o"
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/common/colormodel.cpp" "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/CMakeFiles/SkeletonProject.dir/common/colormodel.cpp.o"
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/common/light.cpp" "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/CMakeFiles/SkeletonProject.dir/common/light.cpp.o"
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/common/model.cpp" "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/CMakeFiles/SkeletonProject.dir/common/model.cpp.o"
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/common/objloader.cpp" "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/CMakeFiles/SkeletonProject.dir/common/objloader.cpp.o"
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/common/polygonmodel.cpp" "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/CMakeFiles/SkeletonProject.dir/common/polygonmodel.cpp.o"
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/common/shader.cpp" "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/CMakeFiles/SkeletonProject.dir/common/shader.cpp.o"
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/common/shadowmap.cpp" "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/CMakeFiles/SkeletonProject.dir/common/shadowmap.cpp.o"
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/common/texture.cpp" "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/CMakeFiles/SkeletonProject.dir/common/texture.cpp.o"
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/common/texturemodel.cpp" "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/CMakeFiles/SkeletonProject.dir/common/texturemodel.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLEW_STATIC"
  "TW_NO_DIRECT3D"
  "TW_NO_LIB_PRAGMA"
  "TW_STATIC"
  "_CRT_SECURE_NO_WARNINGS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../ExternalProjects/glfw-3.2.1/include/GLFW"
  "../ExternalProjects/glm-0.9.8.5"
  "../ExternalProjects/glew-1.9.0/include"
  "../ExternalProjects/soil-1.16/inc"
  "../ExternalProjects/assimp-3.3.1/include"
  "../ExternalProjects/tinyobjloader-1.0.6"
  "../."
  "../ExternalProjects/glfw-3.2.1/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/ExternalProjects/glfw-3.2.1/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/ExternalProjects/CMakeFiles/GLEW_190.dir/DependInfo.cmake"
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/ExternalProjects/soil-1.16/CMakeFiles/soil.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
