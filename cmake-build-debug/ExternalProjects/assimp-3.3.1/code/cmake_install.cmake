# Install script for directory: /Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/ExternalProjects/assimp-3.3.1/code/libassimpd.a")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimpd.a" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimpd.a")
    execute_process(COMMAND "/Library/Developer/CommandLineTools/usr/bin/ranlib" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimpd.a")
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "assimp-dev" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp" TYPE FILE FILES
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/anim.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/ai_assert.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/camera.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/color4.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/color4.inl"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/config.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/defs.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/cfileio.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/light.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/material.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/material.inl"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/matrix3x3.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/matrix3x3.inl"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/matrix4x4.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/matrix4x4.inl"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/mesh.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/postprocess.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/quaternion.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/quaternion.inl"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/scene.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/metadata.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/texture.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/types.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/vector2.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/vector2.inl"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/vector3.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/vector3.inl"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/version.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/cimport.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/importerdesc.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/Importer.hpp"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/DefaultLogger.hpp"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/ProgressHandler.hpp"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/IOStream.hpp"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/IOSystem.hpp"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/Logger.hpp"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/LogStream.hpp"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/NullLogger.hpp"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/cexport.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/Exporter.hpp"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "assimp-dev" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp/Compiler" TYPE FILE FILES
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/Compiler/pushpack1.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/Compiler/poppack1.h"
    "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/assimp-3.3.1/code/../include/assimp/Compiler/pstdint.h"
    )
endif()

