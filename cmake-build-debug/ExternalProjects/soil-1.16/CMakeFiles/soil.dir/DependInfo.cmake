# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/soil-1.16/src/SOIL.c" "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/ExternalProjects/soil-1.16/CMakeFiles/soil.dir/src/SOIL.c.o"
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/soil-1.16/src/image_DXT.c" "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/ExternalProjects/soil-1.16/CMakeFiles/soil.dir/src/image_DXT.c.o"
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/soil-1.16/src/image_helper.c" "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/ExternalProjects/soil-1.16/CMakeFiles/soil.dir/src/image_helper.c.o"
  "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/ExternalProjects/soil-1.16/src/stb_image_aug.c" "/Users/jaeyounglee/Documents/Computer Graphics-2018-s/Homework3/cmake-build-debug/ExternalProjects/soil-1.16/CMakeFiles/soil.dir/src/stb_image_aug.c.o"
  )
set(CMAKE_C_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GLEW_STATIC"
  "TW_NO_DIRECT3D"
  "TW_NO_LIB_PRAGMA"
  "TW_STATIC"
  "_CRT_SECURE_NO_WARNINGS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../ExternalProjects/glfw-3.2.1/include/GLFW"
  "../ExternalProjects/glew-1.9.0/include"
  "../ExternalProjects/soil-1.16/inc"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
