#version 330 core

in vec3 fragmentPosition;
in vec3 fragmentNormal;
// Interpolated values from the vertex shaders
in vec4 fragmentColor;

// Ouput data
out vec4 color;

struct Light
{
    vec3 position;
    vec3 color;
    vec3 direction;
    int l_type;
    int off_on;
    float attenuation;
};

uniform Light lightSources[3];

uniform float ambient_c;
uniform float diffuse_c;
uniform float specular_c;
uniform float shineness;

const float PI = 3.1415926535897932384626433832795f;

uniform mat4 ModelTransform;
uniform mat4 Eye;
uniform mat4 Projection;

void main()
{
	vec3 intensity;
    vec3 normal = normalize(fragmentNormal);
    vec3 ambient_light = vec3(0.5f, 0.5f, 0.5f);

	//color = totalLighting;
    vec3 ambientL = fragmentColor.xyz * ambient_light;

    vec3 light_contribution = vec3(0.0f);

    for (int i = 0; i < 3; i++)
    {
        if(lightSources[i].off_on != 0)
        {
            vec3 normal = normalize(fragmentNormal);
            vec3 light_pos = (inverse(Eye) * vec4(lightSources[i].position, 1)).xyz;
            vec3 light_direction = normalize((inverse(Eye) * vec4(lightSources[i].direction, 0)).xyz);

            vec3 tolight = light_pos - fragmentPosition;
            float distance = abs(length(tolight));

            tolight = normalize(tolight);
            vec3 dir = normalize(2 * dot(tolight, normal) * normal - tolight);
            vec3 half_angle_dir = normalize(tolight + -(fragmentPosition));

            float attenuation_c = 1.0f;

            if (i != 0)
            {
                attenuation_c = attenuation_c / abs(lightSources[i].attenuation * distance);// (lightSources[i].attenuation * distance);

                if (i == 2)
                {
                    float angle = degrees(acos(abs(dot(light_direction, tolight))));
                    if (angle >= 15.0f)
                        attenuation_c = 0.0f;
                }
            }

            vec3 diffuse = attenuation_c * diffuse_c * max(0.0f, dot(normal, tolight)) * lightSources[i].color;

            float specular_div = 4 * dot(-fragmentPosition, fragmentNormal) * dot(fragmentNormal, tolight);

            float alpha = acos(dot(fragmentNormal, half_angle_dir));

            float D = exp(pow(-tan(alpha), 2) / 4.0f) / (PI * 4.0f * pow(cos(alpha), 4));

            float R0 = (10.0f - 1.0f / 10.0f + 1.0f);
            float F = R0 + (1 - R0) * pow((1 - dot(fragmentNormal, tolight)), 5);
            float G = min(1,
                      min(2 * dot(half_angle_dir, fragmentNormal) * dot(-fragmentPosition, fragmentNormal) / dot(-fragmentPosition, half_angle_dir),
                          2 * dot(half_angle_dir, fragmentNormal) * dot(tolight, fragmentNormal) / dot(-fragmentPosition, half_angle_dir)));

            vec3 specular = attenuation_c * specular_c * (D * F * G / specular_div) * lightSources[i].color;

            light_contribution += diffuse + specular;
        }
    }

    vec3 m_color = ambientL + light_contribution;
    color = vec4(m_color.r, m_color.g, m_color.b, fragmentColor.a);
}