// Include standard headers
#include <iostream>
#include <vector>
#include <string>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Include helper
#include "tiny_obj_loader.h"


// Include common libraries
#include <common/geometry.hpp>
#include <common/colormodel.hpp>
#include <common/polygonmodel.hpp>
#include <common/objloader.hpp>
#include <common/shader.hpp>
#include <common/light.hpp>
#include <common/texturemodel.hpp>

using namespace std;
using namespace glm;

enum GEOMETRY_TYPE {
	CUBE,
	BIG_CUBE,
	SPHERE,
	CYLINDER,
	PYRAMID,
	FLAT
};

// Global variable
GLFWwindow* g_window;
float g_window_width = 1024.f;
float g_window_height = 768.f;
int g_framebuffer_width = 1024;
int g_framebuffer_height = 768;
float g_fov = 45.f; // 45-degree of fov
float g_fovy = g_fov; // y-angle of fov

int g_nodes_index = 0;
int g_lights_index = 0;

// Node and Models
vector<ColorModel*> g_light_models;
vector<Light*> g_lights;
vector<TextureModel*> g_texmodels;

// Transformations
mat4 g_projection;
// Camera model property
mat4 g_eye_rbt;
mat4 g_eye_rbt_floor;
mat4 g_eye_rbt_back;
// World model property
mat4 g_world_rbt = mat4(1.0f);


// Event functions
static void WindowSizeCallback(GLFWwindow*, int, int);
static void MouseButtonCallback(GLFWwindow*, int, int, int);
static void CursorPosCallback(GLFWwindow*, double, double);
static void KeyboardCallback(GLFWwindow*, int, int, int, int);
void UpdateFovy(void);
void CleanUp(void);
void AddColorModel(ColorModel& a_model, GLuint a_program, GEOMETRY_TYPE a_type, vec4 a_color);
void UpdateEyeRBT();

// Helper function: Update the vertical field-of-view(float fovy in global)
void UpdateFovy()
{
	if (g_framebuffer_width >= g_framebuffer_height)
	{
		g_fovy = g_fov;
	}
	else {
		const float RAD_PER_DEG = 0.5f * glm::pi<float>() / 180.0f;
		g_fovy = (float)atan2(sin(g_fov * RAD_PER_DEG) * ((float)g_framebuffer_height / (float)g_framebuffer_width), cos(g_fov * RAD_PER_DEG)) / RAD_PER_DEG;
	}
}

void AddColorModel(ColorModel& a_model, GLuint a_program, GEOMETRY_TYPE a_type, vec4 a_color)
{
    /*
     * Setting a model
     * 1. Initialize data for model;
     * 2. Transfer to vertex buffer
     * 3. Set projection, eye matrix, model matrix (in this case we will cover in node class), glsl program
     */
    switch (a_type)
    {
		case CUBE:
			InitDataCube(a_model, a_color);
			a_model.InitializeGLSL(ARRAY);
			break;
		case BIG_CUBE:
			InitDataBigCube(a_model, a_color);
			a_model.InitializeGLSL(ARRAY);
			break;
        case SPHERE:
            InitDataSphere(a_model, a_color);
            a_model.InitializeGLSL(INDEX);
            break;
        case CYLINDER:
            InitDataCylinder(a_model, a_color);
            a_model.InitializeGLSL(ARRAY);
            break;
        case PYRAMID:
            InitDataPyramid(a_model, a_color);
            a_model.InitializeGLSL(ARRAY);
            break;
    }

    a_model.SetProjection(&g_projection);
    a_model.SetEyeRbt(&g_eye_rbt);
    a_model.SetProgram(a_program);
}

void UpdateEyeRBT(mat4* _this, mat4* _tothis)
{
	mat4 temp;
	memcpy(&temp, _this, sizeof(mat4));
	memcpy(_this, _tothis, sizeof(mat4));
	memcpy(_tothis, &temp, sizeof(mat4));
}


// TODO: Modify GLFW window resized callback function
static void WindowSizeCallback(GLFWwindow* a_window, int a_width, int a_height)
{
	// Get resized size and set to current window
	g_window_width = (float)a_width;
	g_window_height = (float)a_height;

	// glViewport accept pixel size, please use glfwGetFramebufferSize rather than window size.
	// window size != framebuffer size
	glfwGetFramebufferSize(a_window, &g_framebuffer_width, &g_framebuffer_height);
	glViewport(0, 0, (GLsizei)g_framebuffer_width, (GLsizei)g_framebuffer_height);
	
	// Update projection matrix
	g_projection = perspective(g_fov, ((float)g_framebuffer_width / (float)g_framebuffer_height), 0.1f, 100.0f);
}

// TODO: Fill up GLFW mouse button callback function
static void MouseButtonCallback(GLFWwindow* a_window, int a_button, int a_action, int a_mods)
{
	//example code for get x position and y position of mouse click
	if (a_button == GLFW_MOUSE_BUTTON_LEFT && a_action == GLFW_PRESS)
	{
		double xpos, ypos;
		glfwGetCursorPos(a_window, &xpos, &ypos);
		xpos = xpos / ((double)g_window_width) * ((double)g_framebuffer_width);
		ypos = ypos / ((double)g_window_height) * ((double)g_framebuffer_height);
	}
}

// TODO: Fill up GLFW cursor position callback function
static void CursorPosCallback(GLFWwindow* a_window, double a_xpos, double a_ypos)
{

}

static void KeyboardCallback(GLFWwindow* a_window, int a_key, int a_scancode, int a_action, int a_mods)
{
	if (a_action == GLFW_PRESS)
	{
		switch (a_key)
		{
		case GLFW_KEY_H:
			cout << "CS580 Homework Assignment 1" << endl;
			cout << "keymaps:" << endl;
			cout << "h\t\t Help command" << endl;
			cout << "p\t\t Enable/Disable picking" << endl;
			break;
		case GLFW_KEY_UP:
			g_eye_rbt = translate(vec3(0.0f, 1.0f, 0.0f)) * g_eye_rbt;
			break;
		case GLFW_KEY_DOWN:
			g_eye_rbt = translate(vec3(0.0f, -1.0f, 0.0f)) * g_eye_rbt;
			break;
		case GLFW_KEY_LEFT:
			g_eye_rbt = translate(vec3(-1.0f, 0.0f, 0.0f)) * g_eye_rbt;
			break;
		case GLFW_KEY_RIGHT:
			g_eye_rbt = translate(vec3(1.0f, 0.0f, 0.0f)) * g_eye_rbt;
			break;
		case GLFW_KEY_LEFT_BRACKET:
			g_eye_rbt = translate(vec3(0.0f, 0.0f, -1.0f)) * g_eye_rbt;
			break;
		case GLFW_KEY_RIGHT_BRACKET:
			g_eye_rbt = translate(vec3(0.0f, 0.0f, 1.0f)) * g_eye_rbt;
			break;
		case GLFW_KEY_A:
			g_lights[0]->TurnOnOff();
			break;
		case GLFW_KEY_S:
            g_lights[1]->TurnOnOff();
			break;
        case GLFW_KEY_D:
            g_lights[2]->TurnOnOff();
			break;
        case GLFW_KEY_J:
            g_lights[g_lights_index]->AddSpeed(4.0f);
            break;
		case GLFW_KEY_K:
			g_lights[g_lights_index]->AddSpeed(-4.0f);
			break;
		case GLFW_KEY_L:
			g_lights_index = (g_lights_index + 1) % g_lights.size();
			break;
		default:
			break;
		}
	}
}


int main( void )
{
	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// Open a window and create its OpenGL context
	g_window = glfwCreateWindow( 1024, 768, "Homework 2: Your Student ID - Your Name", NULL, NULL);

	if( g_window == NULL ){
		fprintf( stderr, "Failed to open GLFW window." );
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(g_window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(g_window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetWindowSizeCallback(g_window, WindowSizeCallback);
	glfwSetMouseButtonCallback(g_window, MouseButtonCallback);
	glfwSetCursorPosCallback(g_window, CursorPosCallback);
	glfwSetKeyCallback(g_window, KeyboardCallback);

	// Clear with black backround
	glClearColor((GLclampf)0.0f, (GLclampf)0.0f, (GLclampf)0.0f, (GLclampf) 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);
	// Enable culling
	glEnable(GL_CULL_FACE);
	// Backface culling
	glCullFace(GL_BACK);

	// Setting viewport
	// glViewport accept pixel size, please use glfwGetFramebufferSize rather than window size.
	// window size != framebuffer size
	glfwGetFramebufferSize(g_window, &g_framebuffer_width, &g_framebuffer_height);
	glViewport(0, 0, (GLsizei)g_framebuffer_width, (GLsizei)g_framebuffer_height);

	g_projection = perspective(g_fov, ((float)g_framebuffer_width / (float)g_framebuffer_height), 0.1f, 100.0f);

	g_eye_rbt = translate(mat4(1.0f), vec3(0.0, 2.0f, 10.0));

    /// 로테이션을 제발 하자!!!!

	vec3 eyeRotate_floor(pi<float>() / 2.0f, 0.0f, 0.0f);
	quat eyeQuaternion_f = quat(eyeRotate_floor);
	mat4 eyeRotation_f = toMat4(eyeQuaternion_f);

	vec3 eyeRotate_y(0.0f, pi<float>(), 0.0f);
	quat eyeQuaternion_y = quat(eyeRotate_y);
	mat4 eyeRotation_y = toMat4(eyeQuaternion_y);


	g_eye_rbt_floor = eyeRotation_f * translate(mat4(1.0f), vec3(0.0, 0.0f, 10.0));
	g_eye_rbt_back = eyeRotation_y * translate(mat4(1.0f), vec3(0.0, -1.0f, 8.0));

	// Initialize shader program (for efficiency)
	GLuint diffuse_shader = LoadShaders("VertexShader.glsl", "FragmentShader.glsl");
	GLuint diffuse_shader_CT = LoadShaders("VertexShader.glsl", "FragmentShader-CT.glsl");
	GLuint diffuse_shader_simple = LoadShaders("VertexShader.glsl", "FragmentShader-Simple.glsl");
	GLuint npr_shader = LoadShaders("VertexShaderNPR.glsl", "FragmentShaderNPR.glsl");
	GLuint line_shader = LoadShaders("LineVertexShader.glsl", "LineFragmentShader.glsl");
    GLuint texture_shader = LoadShaders("TextureVertexShader.glsl", "TextureFragmentShader.glsl");


    /*
     * Light sources configuration
     * 1. Setting position.
     * 2. Setting type.
     * 3. Setting default color of light.
     * 4. Input this light to array.
     * 5. Pass to FragmentShader.glsl.
     */

	Light directional_light = Light(DIRECTIONAL, vec3(1.0f, 1.0f, 1.0f), 1);
	directional_light.SetProperties(translate(mat4(1.0f), vec3(0.0f, 0.0f, -10.0f)) * rotate(mat4(1.0f), radians(30.0f), vec3(1.0f, 0.0f, 0.0f)), 0);


	Light point_light = Light(POINT, vec3(1.0f, 1.0f, 1.0f), 1);
	point_light.SetProperties(translate(mat4(1.0f), vec3(0.0f, 0.0f, -6.0f)) * rotate(mat4(1.0f), radians(30.0f), vec3(0.0f, 1.0f, 0.0f)), 0.03f);

	Light spotlight = Light(SPOT, vec3(1.0f, 1.0f, 1.0f), 1);
	spotlight.SetProperties(translate(mat4(1.0f), vec3(0.0f, 0.0f, -6.0f)) * rotate(mat4(1.0f), radians(-30.0f), vec3(0.0f, 1.0f, 0.0f)), 0.03f);


    g_lights.push_back(&directional_light);
    g_lights.push_back(&point_light);
    g_lights.push_back(&spotlight);

    ColorModel l_model_d = ColorModel();
    l_model_d.SetModelRbt(directional_light.GetLightRbt());
    AddColorModel(l_model_d, diffuse_shader_simple, CUBE, vec4(1.0f, 0.0f, 0.0f, 1.0f));


	ColorModel l_model_p = ColorModel();
	l_model_p.SetModelRbt(point_light.GetLightRbt());
	AddColorModel(l_model_p, diffuse_shader_simple, CUBE, vec4(0.0f, 1.0f, 0.0f, 1.0f));


	ColorModel l_model_s = ColorModel();
	l_model_s.SetModelRbt(spotlight.GetLightRbt());
	AddColorModel(l_model_s, diffuse_shader_simple, CUBE, vec4(0.0f, 0.0f, 1.0f, 1.0f));

    g_light_models.push_back(&l_model_d);
	g_light_models.push_back(&l_model_p);
	g_light_models.push_back(&l_model_s);


    PolygonModel bunny = PolygonModel();
    mat4 bunnyRbt = scale(vec3(10.0f, 10.0f, 10.0f));;
    bunny.LoadModel("../Resources/models/stanford_rabbit_uvs.obj");
    bunny.InitializeGLSL(ARRAY);
    bunny.SetModelRbt(&bunnyRbt);
    bunny.SetEyeRbt(&g_eye_rbt);
    bunny.SetProjection(&g_projection);
    bunny.SetProgram(npr_shader);
	bunny.SetCoefficients(0.2f, 0.0f, 0.0f);
    bunny.LightSources(&g_lights);
    bunny.ActivateTexture();


    ColorModel simple_cube = ColorModel();
    mat4 center = scale(vec3(20.0f, 20.0f, 20.0f));
    simple_cube.SetModelRbt(&center);
    simple_cube.LightSources(&g_lights);
    simple_cube.SetCoefficients(0.2f, 0, 0);
    AddColorModel(simple_cube, diffuse_shader_simple, BIG_CUBE, vec4(1.0f, 1.0f, 1.0f, 0.5f));

	double prev_time = glfwGetTime();

	do {
		// Clear the screen	
		// Fill the background
		
		double current_time = glfwGetTime();
		double elapsed_time = current_time - prev_time;
				
		prev_time = current_time;

        for (int i = 0; i < g_lights.size(); i++)
        {
            g_lights[i]->PositionUpdate(elapsed_time);
            g_light_models[i]->SetModelRbt(g_lights[i]->GetLightRbt());
        }
		
		// Scene update

		// Make Shadow map


        // Draw to Screen
		glClearColor((GLclampf)0.0f, (GLclampf)0.0f, (GLclampf)0.0f, (GLclampf)0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glBindTexture(GL_TEXTURE_2D, 0);


        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        simple_cube.Draw();
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		simple_cube.Draw();

		for (int i = 0; i < g_light_models.size(); i++)
		{
			if (g_lights[i]->CheckOnOff())
			{
                if(i == g_lights_index)
                {
                    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                    g_light_models[i]->Draw();
                    //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
                }
                else
				{
					g_light_models[i]->Draw();
				}

			}
		}

        bunny.Draw();


		// Swap buffers
		glfwSwapBuffers(g_window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(g_window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
	glfwWindowShouldClose(g_window) == 0);

	// Clean up shader
	glDeleteProgram(diffuse_shader);
    glDeleteProgram(diffuse_shader_CT);
    glDeleteProgram(diffuse_shader_simple);
    glDeleteProgram(texture_shader);
	glDeleteProgram(line_shader);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}
