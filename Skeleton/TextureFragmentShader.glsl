#version 330 core

in vec3 fragmentPosition;
in vec3 fragmentNormal;
// Interpolated values from the vertex shaders
in vec2 fragmentUV;

// Ouput data
out vec3 color;

uniform mat4 ModelTransform;
uniform mat4 Eye;
uniform mat4 Projection;

uniform sampler2D tex;
float c = 0.1f;
uniform int mirror;

void main()
{
    vec3 intensity;

    if (mirror == 0)
	    intensity = c * texture(tex, fragmentUV).xyz;
	else if (mirror == 1)
	{
	    vec2 mirror_fragmentUV = vec2(fragmentUV.x, 1 - fragmentUV.y);
	    intensity = c * texture(tex, mirror_fragmentUV).xyz;
	}
	else if (mirror == 2)
	{
	    vec2 mirror_fragmentUV = vec2(1 - fragmentUV.x, fragmentUV.y);
	    intensity = c * texture(tex, mirror_fragmentUV).xyz;
	}

	color = pow(intensity, vec3(1.0/2.2)); // Apply gamma correction
	//color = vec3(1.0f, 1.0f, 1.0f);
}