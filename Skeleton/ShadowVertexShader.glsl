#version 120

layout(location = 0) in vec3 vertexPosition_modelspace;


out vec3 fragmentPosition;

uniform mat4 ModelTransform;
uniform mat4 Eye;
uniform mat4 Projection;

void main()
{
    mat4 MVM = inverse(Eye) * ModelTransform;

    vec4 wPosition = MVM * vec4(vertexPosition_modelspace,1);
    fragmentPosition = wPosition.xyz;
}
