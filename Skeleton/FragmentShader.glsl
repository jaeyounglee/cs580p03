#version 330 core

in vec3 fragmentPosition;
in vec3 fragmentNormal;
// Interpolated values from the vertex shaders
in vec4 fragmentColor;

// Ouput data
out vec4 color;

struct Light
{
    vec3 position;
    vec3 color;
    vec3 direction;
    int l_type;
    int off_on;
    float attenuation;
};

uniform float ambient_c;
uniform float diffuse_c;
uniform float specular_c;
uniform float shineness;

uniform Light lightSources[3];
uniform mat4 ModelTransform;
uniform mat4 Eye;
uniform mat4 Projection;

void main()
{
	vec3 intensity;
    vec3 normal = normalize(fragmentNormal);
    vec3 ambient_light = vec3(0.5f, 0.5f, 0.5f);

	//color = totalLighting;
    vec3 ambientL = fragmentColor.xyz * ambient_light;

    vec3 light_contribution = vec3(0.0f);

    for (int i = 0; i < 3; i++)
    {
        if(lightSources[i].off_on != 0)
        {
            vec3 normal = normalize(fragmentNormal);
            vec3 light_pos = (inverse(Eye) * vec4(lightSources[i].position, 1)).xyz;
            vec3 light_direction = normalize((inverse(Eye) * vec4(lightSources[i].direction, 0)).xyz);

            vec3 tolight = light_pos - fragmentPosition;
            float distance = abs(length(tolight));

            tolight = normalize(tolight);
            vec3 dir = normalize(2 * dot(tolight, normal) * normal - tolight);
            vec3 half_angle_dir = normalize(tolight + -(fragmentPosition));

            float attenuation_c = 1.0f;

            if (i != 0)
            {
                attenuation_c = attenuation_c / abs(lightSources[i].attenuation * distance);// (lightSources[i].attenuation * distance);

                if (i == 2)
                {
                    float angle = degrees(acos(abs(dot(light_direction, tolight))));
                    if (angle >= 15.0f)
                        attenuation_c = 0.0f;
                }
            }

            vec3 diffuse = attenuation_c * diffuse_c * max(0.0f, dot(normal, tolight)) * lightSources[i].color;
            vec3 specular = attenuation_c * specular_c * pow(dot(half_angle_dir, -fragmentPosition), shineness) * lightSources[i].color;

            light_contribution += diffuse + specular;
        }
    }

    vec3 m_color = ambientL + light_contribution;
    color = vec4(m_color.r, m_color.g, m_color.b, fragmentColor.a);
}