#version 330 core

in vec3 fragmentPosition;
in vec3 fragmentNormal;
// Interpolated values from the vertex shaders
in vec4 fragmentColor;

// Ouput data
out vec4 color;

struct Light
{
    vec3 position;
    vec3 color;
    vec3 direction;
    int l_type;
    int off_on;
    float attenuation;
};
uniform Light lightSources[3];
uniform mat4 ModelTransform;
uniform mat4 Eye;
uniform mat4 Projection;

void main()
{
    //color = vec3(1.0, 1.0, 1.0);
    color = fragmentColor;
}