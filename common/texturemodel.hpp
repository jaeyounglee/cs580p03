#ifndef TEXTUREMODEL_HPP
#define TEXTUREMODEL_HPP

#include <common/model.hpp>
#include <common/light.hpp>
#include <common/texture.hpp>

using namespace std;
using namespace glm;

class Light;

class TextureModel : public Model {
protected:
    // vector<vec3> m_positions from Model
    vector<vec3> m_normals;
    vector<vec2> m_uvs;

    GLuint m_normal_buffer_id;
    GLuint m_uv_buffer_id;

    Texture* m_color_texture;

    int mirror;
public:
    // Class constructor
    TextureModel(int);
    // Add vertex normal in (x,y,z) coordinate in model space
    void AddNormal(float a_nx, float a_ny, float a_nz);
    // Add vertex normal in (x,y,z) glm vector in model space
    void AddNormal(vec3 a_normal);
    void AddUV(float u, float v);
    void AddUV(vec2 uv);
    //
    void SetColorTexture(Texture* _texture);
    // Set a shader program for this model and transfer data to vertex buffers
    void InitializeGLSL(DRAW_TYPE a_draw_type);
    // Draw function
    void Draw(void);
    // Clean up all resources
    void CleanUp(void);
};

#endif
