#include <iostream>
#include <vector>
#include <sstream>

#include "colormodel.hpp"

using namespace std;
using namespace glm;

ColorModel::ColorModel()
{
    m_positions = vector<vec3>();
    m_indices = vector<unsigned int>();
    m_normals = vector<vec3>();
    m_colors = vector<vec4>();
    lights = NULL;
}

void ColorModel::LightSources(vector<Light *> *l)
{
    lights = l;
}

void ColorModel::SetShadowProgram(GLuint p_id)
{
    m_glsl_program_id_shadow = p_id;
}

void ColorModel::SetCoefficients(float d, float s, float shiny)
{
    diffuse_coefficient = d;
    specular_coefficient = s;
    shineness = shiny;
}

void ColorModel::AddNormal(float a_nx, float a_ny, float a_nz)
{
    m_normals.push_back(vec3(a_nx, a_ny, a_nz));
}

void ColorModel::AddNormal(vec3 a_normal)
{
    m_normals.push_back(a_normal);
}

void ColorModel::AddColor(float a_r, float a_g, float a_b, float a)
{
    m_colors.push_back(vec4(a_r, a_g, a_b, a));
}

void ColorModel::AddColor(vec4 a_color)
{
    m_colors.push_back(a_color);
}

void ColorModel::InitializeGLSL(DRAW_TYPE a_draw_type)
{
    glUseProgram(m_glsl_program_id);
    m_draw_type = a_draw_type;

    glGenVertexArrays(1, &m_vertex_array_id);
    glBindVertexArray(m_vertex_array_id);

    glGenBuffers(1, &m_position_buffer_id);
    glBindBuffer(GL_ARRAY_BUFFER, m_position_buffer_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)*m_positions.size(), &m_positions[0], GL_STATIC_DRAW);

    if (m_draw_type == DRAW_TYPE::INDEX)
    {
        glGenBuffers(1, &m_index_buffer_id);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer_id);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)*m_indices.size(), &m_indices[0], GL_STATIC_DRAW);
    }

    glGenBuffers(1, &m_normal_buffer_id);
    glBindBuffer(GL_ARRAY_BUFFER, m_normal_buffer_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)*m_normals.size(), &m_normals[0], GL_STATIC_DRAW);

    glGenBuffers(1, &m_color_buffer_id);
    glBindBuffer(GL_ARRAY_BUFFER, m_color_buffer_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec4)*m_colors.size(), &m_colors[0], GL_STATIC_DRAW);
}

void ColorModel::Draw(void)
{
    glUseProgram(m_glsl_program_id);
    GLint projection_id = glGetUniformLocation(m_glsl_program_id, "Projection");
    GLint eye_id = glGetUniformLocation(m_glsl_program_id, "Eye");
    GLint model_id = glGetUniformLocation(m_glsl_program_id, "ModelTransform");


    if (lights != NULL)
    {
        for (int i = 0; i < lights->size(); i++)
            (*lights)[i]->GPULightSetting(m_glsl_program_id);
    }

    glUniformMatrix4fv(projection_id, 1, GL_FALSE, &(*(m_projection))[0][0]);
    glUniformMatrix4fv(eye_id, 1, GL_FALSE, &(*(m_eye_rbt))[0][0]);
    glUniformMatrix4fv(model_id, 1, GL_FALSE, &(*(m_model_rbt))[0][0]);

    d_id = glGetUniformLocation(m_glsl_program_id, "diffuse_c");
    s_id = glGetUniformLocation(m_glsl_program_id, "specular_c");
    shiny_id = glGetUniformLocation(m_glsl_program_id, "shineness");

    glUniform1f(d_id, diffuse_coefficient);
    glUniform1f(s_id, specular_coefficient);
    glUniform1f(shiny_id, shineness);

    glBindVertexArray(m_vertex_array_id);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, m_position_buffer_id);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), ((GLvoid*)(0)));

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, m_normal_buffer_id);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), ((GLvoid*)(0)));

    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, m_color_buffer_id);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(vec4), ((GLvoid*)(0)));

    if (m_draw_type == DRAW_TYPE::ARRAY)
    {
        glDrawArrays(GL_TRIANGLES, 0, (GLsizei)m_positions.size());
    }
    else {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer_id);
        glDrawElements(GL_TRIANGLES, (GLsizei)m_indices.size(), GL_UNSIGNED_INT, ((GLvoid *)0));
    }
}

void ColorModel::Draw_shadow()
{
    glUseProgram(m_glsl_program_id_shadow);
    GLint projection_id = glGetUniformLocation(m_glsl_program_id, "Projection");
    GLint eye_id = glGetUniformLocation(m_glsl_program_id, "Eye");
    GLint model_id = glGetUniformLocation(m_glsl_program_id, "ModelTransform");

    glUniformMatrix4fv(projection_id, 1, GL_FALSE, &(*(m_projection))[0][0]);
    glUniformMatrix4fv(eye_id, 1, GL_FALSE, &(*(m_eye_rbt))[0][0]);
    glUniformMatrix4fv(model_id, 1, GL_FALSE, &(*(m_model_rbt))[0][0]);

    glBindVertexArray(m_vertex_array_id);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, m_position_buffer_id);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), ((GLvoid*)(0)));
}

void ColorModel::CleanUp(void)
{
    // Clean up data structures
    m_positions.clear();
    m_indices.clear();
    m_normals.clear();
    m_colors.clear();
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);

    // Cleanup VBO and VAO (shader should be removed in the main function)
    glDeleteBuffers(1, &m_vertex_array_id);
    glDeleteBuffers(1, &m_normal_buffer_id);
    glDeleteBuffers(1, &m_color_buffer_id);
    if (m_draw_type == DRAW_TYPE::INDEX) glDeleteBuffers(1, &m_index_buffer_id);
    glDeleteVertexArrays(1, &m_vertex_array_id);
}
