#ifndef __POLYMODEL__
#define __POLYMODEL__

#include <vector>
#include <common/model.hpp>
#include <string>


class Light;

class PolygonModel : public Model
{
protected:
    vector<vec2> m_uvs;
    vector<vec3> m_normals;
    vector<vec4> m_colors;
    GLuint m_normal_buffer_id;
    GLuint m_color_buffer_id;
    GLuint m_texcoord_buffer_id;
    GLuint m_texture_id;
    float diffuse_coefficient;
    float specular_coefficient;
    float shineness;
    vector<Light*> *lights;
    GLuint texIDs[5];
public:
    PolygonModel();
    vector<vec3> GetVertices();
    vector<vec2> GetUVs();
    vector<vec3> GetNormals();
    vector<vec4> GetColors();
    void ActivateTexture();
    void SetCoefficients(float, float, float);
    void LightSources(vector<Light *> *l);
    void AddNormal(vec3 a_normal);
    void AddColor(vec4 a_color);
    void LoadModel(string s);
    void InitializeGLSL(DRAW_TYPE a_draw_type);
    // Draw function (abstract)
    void Draw(void);
    // Clean up all resources (abstract)
    void CleanUp(void);
};

#endif