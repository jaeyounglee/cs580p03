#include <iostream>
#include <vector>
#include <sstream>

#include "texturemodel.hpp"

using namespace std;
using namespace glm;

TextureModel::TextureModel(int _mirror)
{
    m_positions = vector<vec3>();
    m_indices = vector<unsigned int>();
    m_normals = vector<vec3>();
    m_uvs = vector<vec2>();

    mirror = _mirror;
}
void TextureModel::AddNormal(float a_nx, float a_ny, float a_nz)
{
    m_normals.push_back(vec3(a_nx, a_ny, a_nz));
}

void TextureModel::AddNormal(vec3 a_normal)
{
    m_normals.push_back(a_normal);
}

void TextureModel::AddUV(float u, float v)
{
    m_uvs.push_back(vec2(u, v));
}

void TextureModel::AddUV(vec2 uv)
{
    m_uvs.push_back(uv);
}


void TextureModel::SetColorTexture(Texture* _texture)
{
    m_color_texture = _texture;
}

void TextureModel::InitializeGLSL(DRAW_TYPE a_draw_type)
{
    glUseProgram(m_glsl_program_id);
    m_draw_type = a_draw_type;

    glGenVertexArrays(1, &m_vertex_array_id);
    glBindVertexArray(m_vertex_array_id);

    glGenBuffers(1, &m_position_buffer_id);
    glBindBuffer(GL_ARRAY_BUFFER, m_position_buffer_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * m_positions.size(), &m_positions[0], GL_STATIC_DRAW);

    if (m_draw_type == DRAW_TYPE::INDEX)
    {
        glGenBuffers(1, &m_index_buffer_id);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer_id);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)*m_indices.size(), &m_indices[0], GL_STATIC_DRAW);
    }

    glGenBuffers(1, &m_normal_buffer_id);
    glBindBuffer(GL_ARRAY_BUFFER, m_normal_buffer_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * m_normals.size(), &m_normals[0], GL_STATIC_DRAW);

    glGenBuffers(1, &m_uv_buffer_id);
    glBindBuffer(GL_ARRAY_BUFFER, m_uv_buffer_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * m_uvs.size(), &m_uvs[0], GL_STATIC_DRAW);
}

void TextureModel::Draw(void)
{
    glUseProgram(m_glsl_program_id);
    GLint projection_id = glGetUniformLocation(m_glsl_program_id, "Projection");
    GLint eye_id = glGetUniformLocation(m_glsl_program_id, "Eye");
    GLint model_id = glGetUniformLocation(m_glsl_program_id, "ModelTransform");
    GLint mirror_id = glGetUniformLocation(m_glsl_program_id, "mirror");

    glUniformMatrix4fv(projection_id, 1, GL_FALSE, &(*(m_projection))[0][0]);
    glUniformMatrix4fv(eye_id, 1, GL_FALSE, &(*(m_eye_rbt))[0][0]);
    glUniformMatrix4fv(model_id, 1, GL_FALSE, &(*(m_model_rbt))[0][0]);
    glUniform1i(mirror_id, mirror);

    // Setting Texture to model
    glActiveTexture(m_color_texture->GetTexture());
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_color_texture->GetTexture());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glUniform1i(glGetUniformLocation(m_glsl_program_id, "tex"), 0);


    glBindVertexArray(m_vertex_array_id);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, m_position_buffer_id);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), ((GLvoid*)(0)));

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, m_normal_buffer_id);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), ((GLvoid*)(0)));

    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, m_uv_buffer_id);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(vec2), ((GLvoid*)(0)));

    if (m_draw_type == DRAW_TYPE::ARRAY)
    {
        glDrawArrays(GL_TRIANGLES, 0, (GLsizei)m_positions.size());
    }
    else {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer_id);
        glDrawElements(GL_TRIANGLES, (GLsizei)m_indices.size(), GL_UNSIGNED_INT, ((GLvoid *)0));
    }
}

void TextureModel::CleanUp(void)
{
    // Clean up data structures
    m_positions.clear();
    m_indices.clear();
    m_normals.clear();
    m_uvs.clear();
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);

    // Cleanup VBO and VAO (shader should be removed in the main function)
    glDeleteBuffers(1, &m_vertex_array_id);
    glDeleteBuffers(1, &m_normal_buffer_id);
    glDeleteBuffers(1, &m_uv_buffer_id);
    if (m_draw_type == DRAW_TYPE::INDEX) glDeleteBuffers(1, &m_index_buffer_id);
    glDeleteVertexArrays(1, &m_vertex_array_id);
}
