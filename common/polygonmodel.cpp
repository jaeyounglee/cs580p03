#include "polygonmodel.hpp"

#include <iostream>
#include <stdio.h>

#ifndef TINYOBJLOADER_IMPLEMENTATION
#define TINYOBJLOADER_IMPLEMENTATION
#endif

#include "tiny_obj_loader.h"
#include "SOIL/SOIL.h"
#include <common/light.hpp>

using namespace std;
using namespace glm;

PolygonModel::PolygonModel()
{
    m_uvs = vector<vec2>();
    m_normals = vector<vec3>();
    m_colors = vector<vec4>();
    m_positions = vector<vec3>();
}

void PolygonModel::SetCoefficients(float d, float s, float shiny)
{
    diffuse_coefficient = d;
    specular_coefficient = s;
    shineness = shiny;
}

vector<vec3> PolygonModel::GetVertices()
{
    return m_positions;
}

vector<vec2> PolygonModel::GetUVs()
{
    return m_uvs;
}

vector<vec3> PolygonModel::GetNormals()
{
    return m_normals;
}
vector<vec4> PolygonModel::GetColors()
{
    return m_colors;
}

void PolygonModel::AddNormal(vec3 a_normal)
{
    m_normals.push_back(a_normal);
}

void PolygonModel::AddColor(vec4 a_color)
{
    m_colors.push_back(a_color);
}

void PolygonModel::LightSources(vector<Light *> *l)
{
    lights = l;
}

void PolygonModel::LoadModel(string s)
{
    string inputfile = s;
    tinyobj::attrib_t attrib;
    vector<tinyobj::shape_t> shapes;
    vector<tinyobj::material_t> materials;
    string err;

    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, inputfile.c_str());

    if (!err.empty())
    { // `err` may contain warning message.
        cout << "error" << endl;
        cout << err << endl;
    }

    if (!ret)
        exit(1);

    // Loop over shapes
    for (size_t s = 0; s < shapes.size(); s++)
    {
        // Loop over faces(polygon)
        size_t index_offset = 0;
        for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++)
        {
            int fv = shapes[s].mesh.num_face_vertices[f];

            // Loop over vertices in the face.
            for (size_t v = 0; v < fv; v++)
            {
                // access to vertex
                tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
                tinyobj::real_t vx = attrib.vertices[3*idx.vertex_index+0];
                tinyobj::real_t vy = attrib.vertices[3*idx.vertex_index+1];
                tinyobj::real_t vz = attrib.vertices[3*idx.vertex_index+2];
                tinyobj::real_t nx = attrib.normals[3*idx.normal_index+0];
                tinyobj::real_t ny = attrib.normals[3*idx.normal_index+1];
                tinyobj::real_t nz = attrib.normals[3*idx.normal_index+2];
                tinyobj::real_t tx = attrib.texcoords[2*idx.texcoord_index+0];
                tinyobj::real_t ty = attrib.texcoords[2*idx.texcoord_index+1];
                // Optional: vertex colors
                tinyobj::real_t red = 1.0f; //attrib.colors[3*idx.vertex_index+0];
                tinyobj::real_t green = 1.0f; //attrib.colors[3*idx.vertex_index+1];
                tinyobj::real_t blue = 1.0f; //attrib.colors[3*idx.vertex_index+2];
                tinyobj::real_t alpha = 0.9f; //attrib.colors[3*idx.vertex_index+2];

                vec3 pos = vec3(vx, vy, vz);
                vec3 nor = vec3(nx, ny, nz);
                vec2 tc = vec2(tx, ty);
                m_positions.push_back(pos);
                m_normals.push_back(nor);
                m_uvs.push_back(tc);
                m_colors.push_back(vec4(red, green, blue, alpha));
            }
            index_offset += fv;

            // per-face material
            shapes[s].mesh.material_ids[f];
        }
    }

}

void PolygonModel::InitializeGLSL(DRAW_TYPE a_draw_type)
{
    glUseProgram(m_glsl_program_id);
    m_draw_type = a_draw_type;

    glGenVertexArrays(1, &m_vertex_array_id);
    glBindVertexArray(m_vertex_array_id);

    glGenBuffers(1, &m_position_buffer_id);
    glBindBuffer(GL_ARRAY_BUFFER, m_position_buffer_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)*m_positions.size(), &m_positions[0], GL_STATIC_DRAW);

    if (m_draw_type == DRAW_TYPE::INDEX)
    {
        glGenBuffers(1, &m_index_buffer_id);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer_id);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)*m_indices.size(), &m_indices[0], GL_STATIC_DRAW);
    }

    glGenBuffers(1, &m_normal_buffer_id);
    glBindBuffer(GL_ARRAY_BUFFER, m_normal_buffer_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)*m_normals.size(), &m_normals[0], GL_STATIC_DRAW);

    glGenBuffers(1, &m_color_buffer_id);
    glBindBuffer(GL_ARRAY_BUFFER, m_color_buffer_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec4)*m_colors.size(), &m_colors[0], GL_STATIC_DRAW);

    glGenBuffers(1, &m_texcoord_buffer_id);
    glBindBuffer(GL_ARRAY_BUFFER, m_texcoord_buffer_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec2)*m_uvs.size(), &m_uvs[0], GL_STATIC_DRAW);
}


void PolygonModel::ActivateTexture()
{
    int width;
    int height;
    unsigned char *image;

    glGenTextures(5, texIDs);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texIDs[0]);
    image = SOIL_load_image("../Resources/images/tex1.png", &width, &height, 0, SOIL_LOAD_RGB);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    //SOIL_free_image_data(image);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texIDs[1]);
    image = SOIL_load_image("../Resources/images/tex2.png", &width, &height, 0, SOIL_LOAD_RGB);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    SOIL_free_image_data(image);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, texIDs[2]);
    image = SOIL_load_image("../Resources/images/tex3.png", &width, &height, 0, SOIL_LOAD_RGB);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    SOIL_free_image_data(image);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, texIDs[3]);
    image = SOIL_load_image("../Resources/images/tex4.png", &width, &height, 0, SOIL_LOAD_RGB);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    SOIL_free_image_data(image);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, texIDs[4]);
    image = SOIL_load_image("../Resources/images/tex5.png", &width, &height, 0, SOIL_LOAD_RGB);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    SOIL_free_image_data(image);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

}

// Draw function (abstract)
void PolygonModel::Draw(void)
{
    glUseProgram(m_glsl_program_id);
    GLint projection_id = glGetUniformLocation(m_glsl_program_id, "Projection");
    GLint eye_id = glGetUniformLocation(m_glsl_program_id, "Eye");
    GLint model_id = glGetUniformLocation(m_glsl_program_id, "ModelTransform");
    GLuint d_id = glGetUniformLocation(m_glsl_program_id, "diffuse_c");
    GLuint s_id = glGetUniformLocation(m_glsl_program_id, "specular_c");
    GLuint shiny_id = glGetUniformLocation(m_glsl_program_id, "shineness");
    GLuint tex1Loc = glGetUniformLocation(m_glsl_program_id, "tex1");
    GLuint tex2Loc = glGetUniformLocation(m_glsl_program_id, "tex2");
    GLuint tex3Loc = glGetUniformLocation(m_glsl_program_id, "tex3");
    GLuint tex4Loc = glGetUniformLocation(m_glsl_program_id, "tex4");
    GLuint tex5Loc = glGetUniformLocation(m_glsl_program_id, "tex5");

    if (lights != NULL)
    {
        for (int i = 0; i < 3; i++)
        {
            (*lights)[i]->GPULightSetting(m_glsl_program_id);
        }
    }


    glUniformMatrix4fv(projection_id, 1, GL_FALSE, &(*(m_projection))[0][0]);
    glUniformMatrix4fv(eye_id, 1, GL_FALSE, &(*(m_eye_rbt))[0][0]);
    glUniformMatrix4fv(model_id, 1, GL_FALSE, &(*(m_model_rbt))[0][0]);
    glUniform1f(d_id, diffuse_coefficient);
    glUniform1f(s_id, specular_coefficient);
    glUniform1f(shiny_id, shineness);

    glActiveTexture(texIDs[0]);
    glBindTexture(GL_TEXTURE_2D, texIDs[0]);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glUniform1i(tex1Loc, 0);

    glActiveTexture(texIDs[1]);
    glBindTexture(GL_TEXTURE_2D, texIDs[01]);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glUniform1i(tex2Loc, 1);

    glActiveTexture(texIDs[2]);
    glBindTexture(GL_TEXTURE_2D, texIDs[2]);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glUniform1i(tex3Loc, 2);

    glActiveTexture(texIDs[3]);
    glBindTexture(GL_TEXTURE_2D, texIDs[3]);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glUniform1i(tex4Loc, 3);

    glActiveTexture(texIDs[4]);
    glBindTexture(GL_TEXTURE_2D, texIDs[4]);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glUniform1i(tex5Loc, 4);


    glBindVertexArray(m_vertex_array_id);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, m_position_buffer_id);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), ((GLvoid*)(0)));

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, m_normal_buffer_id);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), ((GLvoid*)(0)));

    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, m_color_buffer_id);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(vec4), ((GLvoid*)(0)));

    glEnableVertexAttribArray(3);
    glBindBuffer(GL_ARRAY_BUFFER, m_texcoord_buffer_id);
    glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(vec2), ((GLvoid*)(0)));

    if (m_draw_type == DRAW_TYPE::ARRAY)
    {
        glDrawArrays(GL_TRIANGLES, 0, (GLsizei)m_positions.size());
    }
    else {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer_id);
        glDrawElements(GL_TRIANGLES, (GLsizei)m_indices.size(), GL_UNSIGNED_INT, ((GLvoid *)0));
    }
}

// Clean up all resources (abstract)
void PolygonModel::CleanUp(void)
{
    // Clean up data structures
    m_positions.clear();
    m_indices.clear();
    m_normals.clear();
    m_colors.clear();
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);

    // Cleanup VBO and VAO (shader should be removed in the main function)
    glDeleteBuffers(1, &m_vertex_array_id);
    glDeleteBuffers(1, &m_normal_buffer_id);
    glDeleteBuffers(1, &m_color_buffer_id);
    glDeleteBuffers(1, &m_texcoord_buffer_id);
    if (m_draw_type == DRAW_TYPE::INDEX) glDeleteBuffers(1, &m_index_buffer_id);
}