//
// Created by 이재영 on 2018. 5. 25..
//

#ifndef CS580_SHADOWMAP_HPP
#define CS580_SHADOWMAP_HPP

#include <vector>
#include <common/texture.hpp>
#include <common/light.hpp>

using namespace std;

class ShadowMap
{
protected:
    Texture m_texture;
    GLuint m_fbo;
public:
    ShadowMap();
    void BindFBO();
    void UnbindFBO();
    void CreateTexture(double, double);
    Texture *GetTexture();
};


#endif //CS580_SHADOWMAP_HPP
