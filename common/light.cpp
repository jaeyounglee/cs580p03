// This code is licensed under MIT license (see LICENSE.txt for details)

#include <iostream>
#include <vector>
#include <sstream>
#include <string>

#include "light.hpp"

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace std;
using namespace glm;

Light::Light(LIGHT_TYPE _type, vec3 _color, int i)
{
    l_type = _type;
    color = _color;
    off_on = i;
    l_speed = 0;
}

void Light::SetProperties(mat4 rbt, float a)
{
    l_rbt = rbt;
    attenuation = a;
}

void Light::GPULightSetting(GLuint a_program_id)
{
    uint32_t i = l_type;

    string pos = "lightSources[" + to_string(i) + "].position";
    string col = "lightSources[" + to_string(i) + "].color";
    string type = "lightSources[" + to_string(i) + "].l_type";
    string mode = "lightSources[" + to_string(i) + "].off_on";
    string dir = "lightSources[" + to_string(i) + "].direction";
    string att = "lightSources[" + to_string(i) + "].attenuation";

    GLuint l_position_id = glGetUniformLocation(a_program_id, pos.c_str());
    GLuint l_color_id = glGetUniformLocation(a_program_id, col.c_str());
    GLuint l_light_type_id = glGetUniformLocation(a_program_id, type.c_str());
    GLuint l_off_on_id = glGetUniformLocation(a_program_id, mode.c_str());
    GLuint l_direction_id = glGetUniformLocation(a_program_id, dir.c_str());
    GLuint l_att_id = glGetUniformLocation(a_program_id, att.c_str());

    glUniform3f(l_position_id, position.x, position.y, position.z);
    glUniform3f(l_color_id, color.r, color.g, color.b);
    glUniform3f(l_direction_id, direction.x, direction.y, direction.z);
    glUniform1i(l_light_type_id, l_type);
    glUniform1i(l_off_on_id, off_on);
    glUniform1f(l_att_id, attenuation);
}



void Light::TurnOff()
{
    off_on = 0;
}

void Light::TurnOn()
{
    off_on = 1;
}

void Light::TurnOnOff()
{
    off_on = !off_on;
}

int Light::CheckOnOff()
{
    return off_on;
}

void Light::AddSpeed(float a)
{
    l_speed += a;
}

void Light::PositionUpdate(float elapsed_time)
{
    l_rbt = rotate(mat4(1.0f), radians(l_speed * elapsed_time), vec3(0.0f, 1.0f, 0.0f)) * l_rbt;
    position.x = l_rbt[3][0];
    position.y = l_rbt[3][1];
    position.z = l_rbt[3][2];

    direction = -position;

    //printf("direction = (%f, %f, %f)\n", direction.x, direction.y, direction.z);
    //printf("position = (%f, %f, %f)\n", position.x, position.y, position.z);
}

mat4* Light::GetLightRbt()
{
    return &l_rbt;
}