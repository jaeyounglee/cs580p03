#ifndef LIGHT_HPP
#define LIGHT_HPP

#include <GL/glew.h>
#include <vector>
#include <glm/glm.hpp>
#include <common/model.hpp>

using namespace std;
using namespace glm;

enum LIGHT_TYPE
{
    DIRECTIONAL = 0,
    POINT = 1,
    SPOT = 2
};

class Light
{
protected:
	mat4 l_rbt;
    vec3 position;
    vec3 color;
    vec3 direction;
    LIGHT_TYPE l_type;
    int off_on;
    float attenuation;
	float l_speed;
public:
	Light(LIGHT_TYPE, vec3 color, int);
    void SetProperties(mat4 rbt, float);
    void GPULightSetting(GLuint a_program_id);
    void TurnOff();
    void TurnOn();
	// Set a shader program for this model an
    void TurnOnOff();
    int CheckOnOff();
    void PositionUpdate(float elapsed_time);
	void AddSpeed(float a);
	mat4* GetLightRbt();
};

#endif
