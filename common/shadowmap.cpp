//
// Created by 이재영 on 2018. 5. 25..
//

#include "shadowmap.hpp"

ShadowMap::ShadowMap()
{
    m_texture = Texture();
}

void ShadowMap::BindFBO()
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
}

void ShadowMap::UnbindFBO()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void ShadowMap::CreateTexture(double a_width, double a_height)
{
        GLuint depthTexture;
        glGenTextures(1, &depthTexture);
        glBindTexture(GL_TEXTURE_2D, depthTexture);
        glTexImage2D(GL_TEXTURE_2D, 0,GL_DEPTH_COMPONENT16, a_width, a_height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        GLuint framebuffer;
        glGenFramebuffers(1, &m_fbo);
        glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
        glFramebufferTexture2D(GL_FRAMEBUFFER,
                               GL_COLOR_ATTACHMENT0,
                               GL_TEXTURE_2D,
                               depthTexture,
                               0);
        m_texture.SetTexture(depthTexture);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

Texture *ShadowMap::GetTexture()
{
    return &m_texture;
}
