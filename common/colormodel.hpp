#ifndef COLORMODEL_HPP
#define COLORMODEL_HPP

#include <common/model.hpp>
#include <common/light.hpp>

using namespace std;
using namespace glm;

class Light;

class ColorModel : public Model {
protected:
    // vector<vec3> m_positions from Model
    vector<vec3> m_normals;
    vector<vec4> m_colors;
    GLuint m_normal_buffer_id;
    GLuint m_color_buffer_id;
    GLuint d_id;
    GLuint s_id;
    GLuint shiny_id;

    GLuint m_glsl_program_id_shadow;
    vector<Light*> *lights;
public:
    // Class constructor
    ColorModel();
    void SetShadowProgram(GLuint p_id);
    void LightSources(vector<Light*> *l);
    // Add vertex normal in (x,y,z) coordinate in model space
    void AddNormal(float a_nx, float a_ny, float a_nz);
    void SetCoefficients(float d, float s, float shiny);
    // Add vertex normal in (x,y,z) glm vector in model space
    void AddNormal(vec3 a_normal);
    // Add vertex color in (r,g,b) coordinate
    void AddColor(float a_r, float a_g, float a_b, float a);
    // Add vertex color in (r,g,b) glm vector
    void AddColor(vec4 a_color);
    // Set a shader program for this model and transfer data to vertex buffers
    void InitializeGLSL(DRAW_TYPE a_draw_type);
    // Draw function
    void Draw(void);
    void Draw_shadow(void);
    // Clean up all resources
    void CleanUp(void);
};

#endif
